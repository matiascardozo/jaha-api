const functions = require("firebase-functions");
var nodemailer = require("nodemailer");
const express = require("express");
var bodyParser = require("body-parser");
const fs = require('fs');
let _ = require('underscore')
var cors = require('cors')

const app = express();

app.use(cors())
app.use(bodyParser.json());


app.post("/mail", (req, res) => {
	
	let mailTemplate = fs.readFileSync('./templates/notifiacionChofer.html', 'utf8');

	var compiled = _.template(mailTemplate);
	mailOptions.to = req.body.email
	mailOptions.html = compiled(req.body)
	transporter.sendMail(mailOptions, function(err, info) {
		if (err) res.send(err);
		else res.send('ok');
	});
});


app.post("/notif-nueva-solicitud", (req, res) => {
	
	let mailTemplate = fs.readFileSync('./templates/notifiacionPasajero.html', 'utf8');
	console.log(req.body)
	var compiled = _.template(mailTemplate);
	mailOptions.to = req.body.viaje.chofer.email
	mailOptions.html = compiled(req.body)
	mailOptions.subject = "Jaha notificacion de solicitud"
	transporter.sendMail(mailOptions, function(err, info) {
		if (err) res.send(err);
		else res.send('ok');
	});
});

var transporter = nodemailer.createTransport({
	service: "gmail",
	auth: {
		user: "gestpro.is2@gmail.com",
		pass: "gestpro123456"
	}
});
const mailOptions = {
	from: "gestpro.is2@gmail.com", // sender address
	to: "matiasicardozol@gmail.com", // list of receivers
	subject: "Jaha notificacion de confirmacion", // Subject line
	html: "<p>Your html here</p>", // plain text body,
	attachments: [{
	filename: 'image.png',
	path: 'templates/logo.jpeg',
	cid: 'unique@nodemailer.com' //same cid value as in the html img src
	}]
};

// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions

exports.app = functions.https.onRequest(app);
